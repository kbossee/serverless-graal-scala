pipeline {
    agent { label 'serverless' }

    stages {

        stage('Build') {
            steps {
                sh "sudo yum install zlib-devel -y"
                sh "chmod +x build.sh"
                sh "./build.sh"
            }
        }

        stage('Deploy') {
            steps {
                sh "sls deploy"
            }
        }

    }
    post {
        aborted {
            script {
                currentBuild.result = 'SUCCESS'
            }
        }
    }
}