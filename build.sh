#!/usr/bin/env bash

source download-graal.sh

sbt 'show graalvm-native-image:packageBin'

mv target/graalvm-native-image/serverless-graal-scala target/graalvm-native-image/bootstrap
