

lazy val `serverless-graal-scala` = (project in file("."))
  .enablePlugins(
    GraalVMNativeImagePlugin
  )
  .settings(

    graalVMNativeImageOptions ++= Seq(
      "-H:EnableURLProtocols=http",
      "-Djava.net.preferIPv4Stack=true"
    ),


    libraryDependencies ++= Seq(

      "com.softwaremill.sttp" % "core_2.12" % "1.5.1",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.7",
      "com.fasterxml.jackson.module" % "jackson-module-scala_2.12" % "2.9.7",
    )
  )
